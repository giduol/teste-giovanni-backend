package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ObterProperties {

    public String get(String prop) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("src/main/resources/url.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String value = null;
        for(String key : properties.stringPropertyNames()) {
            value = properties.getProperty(prop);
        }
        return value;
    }
}
