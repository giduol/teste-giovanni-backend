package helper;

import org.testng.annotations.BeforeClass;
import utils.ObterProperties;
import org.testng.annotations.Listeners;
import com.aventstack.extentreports.testng.listener.ExtentITestListenerClassAdapter;
import static io.restassured.RestAssured.enableLoggingOfRequestAndResponseIfValidationFails;

@Listeners(ExtentITestListenerClassAdapter.class)
public class BaseTest {

    protected Steps steps = new Steps();

    @BeforeClass()
    public void baseSetUp() {
        enableLoggingOfRequestAndResponseIfValidationFails();
    }
}
