package helper;

import DTO.Usuario;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import org.json.JSONObject;
import utils.ObterProperties;

import java.util.Map;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import static io.restassured.RestAssured.given;

public class Steps {

    ObterProperties prop = new ObterProperties();
    String loginUri = prop.get("baseUri") + prop.get("basePathLogin");
    String usuariosUri = prop.get("baseUri") + prop.get("basePathUsuarios");

    public String gerarToken(){
        JSONObject login = new JSONObject();
        login.put("email", "fulano@qa.com");
        login.put("password", "teste");

        JsonPath token =
                given()
                    .contentType(ContentType.JSON).
                    log().
                    all().
                when().
                    body(login.toString()).
                    post(loginUri).
                    prettyPeek().
                then().
                    statusCode(200).
                    extract().
                    body().
                    jsonPath();

        return token.getString("authorization");
    }

    public JsonPath inserirCadastro(String token, Usuario objeto) {

        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> objetoMap = objectMapper.convertValue(objeto, Map.class);

        JsonPath response =
                given().
                    contentType(ContentType.JSON).
                    log().
                    all().
                when().
                    header("Authorize", token).
                    body(objetoMap).
                    post(usuariosUri).
                    prettyPeek().
                then().
                    statusCode(201).
                    extract().
                    body().
                    jsonPath();
        return response;
    }

    public String editarCadastro(String id, Usuario objeto) {

        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> objetoMap = objectMapper.convertValue(objeto, Map.class);

        JsonPath response =
                given().
                    contentType(ContentType.JSON).
                    log().
                    all().
                when().
                    body(objetoMap).
                    put(usuariosUri + "/" + id).
                    prettyPeek().
                then().
                    statusCode(200).
                    extract().
                    body().
                    jsonPath();
        return response.getString("message");
    }

    public String excluirCadastro(String id) {
        JsonPath response =
                given().
                    log().
                    all().
                when().
                    delete(usuariosUri + "/" + id).
                    prettyPeek().
                then().
                    statusCode(200).
                    extract().
                    body().
                    jsonPath();
        return response.getString("message");
    }

    public JsonPath buscarCadastro(String id) {
        JsonPath response =
                 given().
                     log().
                     all().
                 when().
                    get(usuariosUri + "/" + id).
                    prettyPeek().
                 then().
                    statusCode(200).
                    extract().
                    body().
                    jsonPath();
        return response;
    }

    public JsonPath buscarListaUsuarios() {
        JsonPath response =
                given().
                    log().
                    all().
                when().
                    get(usuariosUri).
                    prettyPeek().
                then().
                    statusCode(200).
                    extract().
                    body().
                    jsonPath();
        return response;
    }

    public Boolean healthCheck() {
                   try {
                       given().
                           log().
                           all().
                       when().
                           get(usuariosUri).
                           prettyPeek().
                       then().
                           statusCode(200);
                       return true;
                   } catch (Exception e) {
                       e.printStackTrace();
                       return false;
                   }
    }

    public Boolean testeContrato(){
        try {
            given().
                log().all().
                when().
                get(usuariosUri).
                prettyPeek().
                then().
                body(matchesJsonSchemaInClasspath("contrato.json"));
                return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}







