package DTO;


import lombok.Getter;
import lombok.Setter;

public class Usuario {
    @Getter
    @Setter
    private String NOME = "Giovanni";

    @Getter
    @Setter
    private String EMAIL = "giovannioliveira@gmail.com";

    @Getter
    @Setter
    private String PASSWORD = "teste";

    @Getter
    @Setter
    private String ADMINISTRADOR = "true";

}
