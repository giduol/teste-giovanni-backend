package testcases;

import DTO.Usuario;
import helper.BaseTest;
import helper.Steps;
import io.restassured.path.json.JsonPath;
import org.json.JSONObject;
import org.omg.CORBA.UnknownUserException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class EditarUsuario extends BaseTest {

    @Test
    public void realizarEdicao() {
        String token = steps.gerarToken();

        Usuario usuario = new Usuario();

        JsonPath insercao = steps.inserirCadastro(token, usuario);

        usuario.setNOME("Giovanni editado");
        usuario.setEMAIL("giovannioliveiraeditado@gmail.com");

        String mensagemEdicao = steps.editarCadastro(insercao.getString("_id"), usuario);
        Assert.assertEquals(mensagemEdicao, "Registro alterado com sucesso");
        steps.excluirCadastro(insercao.getString("_id"));

    }
}







